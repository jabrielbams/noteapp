package com.jabriel.connectdb

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jabriel.connectdb.databinding.ItemCardNoteVerticalBinding
import com.jabriel.connectdb.room.Note
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NoteAdapter(private var notes: ArrayList<Note>, private val listener: OnAdapterListener) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val binding =
            ItemCardNoteVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoteViewHolder(binding)
    }

    override fun getItemCount() = notes.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.binding.tvTittle.text = note.title
        holder.binding.tvContent.text = note.note

        // Set the pin icon based on the note's pin status
        if (note.isPinned) {
            holder.binding.ibPin.setImageResource(R.drawable.ic_pin_active)
        } else {
            holder.binding.ibPin.setImageResource(R.drawable.ic_pin_inactive)
        }

        holder.binding.ibPin.setOnClickListener {
            // Toggle the pin status
            note.isPinned = !note.isPinned
            listener.onPin(note)

            // Update the notes list
            val sortedNotes = notes.sortedWith(compareByDescending<Note> { it.isPinned }.thenByDescending { it.id })
            setData(sortedNotes)

            // Notify the adapter that the data has changed
            notifyDataSetChanged()
        }

        holder.binding.tvTittle.setOnClickListener {
            listener.onRead(note)
        }

        holder.binding.clTextLayout.setOnClickListener {
            listener.onUpdate(note)
        }

        holder.binding.clTextLayout.setOnLongClickListener {
            listener.onDelete(note)
            vibrateDevice(it.context, 500)
            true
        }

    }

    private fun vibrateDevice(context: Context, duration: Long) {
        val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(duration)
        }
    }
    inner class NoteViewHolder(val binding: ItemCardNoteVerticalBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setData(newList: List<Note>) {
        notes.clear()
        notes.addAll(newList)
        notifyDataSetChanged()
    }

    interface OnAdapterListener {

        fun onRead(note: Note)
        fun onUpdate(note: Note)
        fun onDelete(note: Note)
        fun onPin(note: Note)
    }
}
