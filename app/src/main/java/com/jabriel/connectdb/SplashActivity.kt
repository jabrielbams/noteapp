package com.jabriel.connectdb

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.jabriel.connectdb.databinding.ActivitySplashBinding
import com.jabriel.connectdb.onboarding.OnBoardingActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAction()
    }

    private fun initAction() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@SplashActivity)
        val isFirstOpen = sharedPreferences.getBoolean("isFirstOpen", false)
        GlobalScope.launch {
            delay(3000L)
            if (isFirstOpen) {
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this@SplashActivity, OnBoardingActivity::class.java)
                startActivity(intent)
            }
            finish()
        }
    }
}